import { GENERATE_TOKENS, GET_ENTERPRISES, GET_ERRORS } from '../actions/types'

export interface MenuState {
  enterprises: Enterprise[],
  tokenErrors: CustomError,
  tokens: TokenState
}


const initialState = {
  enterprises: [],
  tokenErrors: {
    message: '',
    statusCode: 0
  },
  tokens: {
    token: '',
    informationCode: '',
    typeOperation: '',
    refreshToken: '',
    urlApp: ''
  }
}



/**
 *  accion de consulta de empresas
 * @param state
 * @param action
 */
export const menuReducer = (state: MenuState = initialState, action: any): MenuState => {
  debugger
  switch (action.type) {
    case GET_ENTERPRISES:
      return {
        ...state,
        enterprises: action.payload,
      }
    case GENERATE_TOKENS:
      return {
        ...state,
        tokens: action.payload
      }
    case GET_ERRORS:
      return {
        ...state,
        tokenErrors: action.payload
      }
    default:
      return state;
  }
}
