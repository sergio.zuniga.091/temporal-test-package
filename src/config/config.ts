export const API_SECURITY_ENDPOINT =
    process.env.NODE_ENV === "production"
        ? "https://jkcge88asd.execute-api.us-east-2.amazonaws.com/qa/security"
        : "https://jkcge88asd.execute-api.us-east-2.amazonaws.com/qa/security";
export const API_AUTHENTICATION =
    process.env.NODE_ENV === "production"
        ? "https://jkcge88asd.execute-api.us-east-2.amazonaws.com/qa/authentication"
        : "https://jkcge88asd.execute-api.us-east-2.amazonaws.com/qa/authentication";
