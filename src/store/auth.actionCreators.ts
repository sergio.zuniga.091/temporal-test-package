import { GENERATE_TOKENS, GET_ERRORS } from '../actions/types'

export const generateTokensResponse = (token: TokenState): TokenResponseAction => ({
    type: GENERATE_TOKENS,
    payload: token
})

export const clearTokenResponse = (): TokenResponseAction => ({
    type: GENERATE_TOKENS,
    payload: {
        token: '',
        informationCode: '',
        typeOperation: '',
        refreshToken: '',
        urlApp: ''
    }
})


export const generateTokensErrors = (error: CustomError): CustomErrorAction => ({
    type: GET_ERRORS,
    payload: error

})