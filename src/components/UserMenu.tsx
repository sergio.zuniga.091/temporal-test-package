import {
  Avatar,
  CircularProgress,
  Divider,
  FormControlLabel,
  Grid,
  InputAdornment,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  makeStyles,
  MenuItem,
  Modal,
  RadioGroup,
  TextField,
  Tooltip,
  Typography,
} from '@material-ui/core'
import { AccountCircle, Business, PowerSettingsNew } from '@material-ui/icons'
import NestedMenuItem from 'material-ui-nested-menu-item'
import React, { useEffect, useState } from 'react'
import { StyledMenu } from './StyledMenu'
import SearchIcon from '@material-ui/icons/Search'
import { useDispatch, useSelector } from 'react-redux'
import { generateTokens } from '../actions/auth.action'
import { getEnterprise } from '../actions/enterprise.action'
import StyledRadioButton from './StyledRadioButton'
import { parseJwt, toPascalCase } from '../utils/utils'
import AlertConfirm from './Alert/AlertConfirm'
import { FC } from 'react'
import { clearTokenResponse, generateTokensErrors } from '../store/auth.actionCreators'
import { RootState } from '../reducers'


const useStyles = makeStyles((theme: any) => ({
  popover: {
    pointerEvents: 'none',
  },
  paper: {
    padding: theme.spacing(1),
  },
  rightButton: {
    padding: theme.spacing(0, 0, 0, 10),
  },
  typography: {
    color: '#6D6E71',
  },
  paperModal: {
    position: 'absolute',
    width: 400,
    title: {
      justifyContent: 'left',
    },
    backgroundColor: theme.palette.common.white,
    border: `2px solid ${theme.palette.secondary.light}`,
    borderRadius: '10px',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  searchTextField: {
    width: "100%",
    padding: "10px 23px 10px 10px",
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderRadius: `10px`,
        //  backgroundColor: theme.palette.common.white,
        height: '40px',
        boxShadow: '0px 3px 2px #00000026',
        border: `1px solid ${theme.palette.info.light}`,
      },
    },
  },
  modalEnterprises: {
    height: '10vh',
    borderColor: theme.palette.secondary.main
  },
  radioList: {
    height: 400,
    overflowY: 'scroll'
  },
  root: (props) => ({
    '&:focus': {
      background: theme.palette.primary.main,
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        color: '#F5F5F5',
      },
    },
    '&:hover': { background: 'blue' },
    width: '250px',
    color: '#A3A3A3',
  }),
  logo: {
    width: '163px',
    backgroundSize: 'contain',
    backgroundRepeat: 'no-repeat',
    height: '43px',
    //   backgroundImage: theme.logo.backgroundImage,
    paddingTop: '14px',
    // flex: 1,
  },
  contentProfile: {
    width: 250,
  },
}))
interface UserMenuProps {
  appName: string,
  userInfo?: UserInfo
}
const UserMenu: FC<UserMenuProps> = ({ appName, userInfo, }) => {
  userInfo = !!userInfo ? userInfo : {
    user: {
      enterprise: '',
      email: '',
      name: '',
      role: [''],
      profilePicture: ''
    },
  }

  const classes = useStyles()
  debugger
  const enterprises = useSelector((state: RootState) => state.menu.enterprises);
  const tokens = useSelector((state: RootState) => state.menu.tokens);
  const tokenErrors = useSelector((state: RootState) => state.menu.tokenErrors);
  const [anchorEl, setAnchorEl] = React.useState<boolean | null>(null)
  const [, setAnchorElPop] = React.useState<boolean | null>(null)
  const openMenu = Boolean(!!anchorEl)
  const [enteredFilter, setEnteredFilter] = useState('')
  const [confirmPrincipal, setConfirmPrincipal] = useState(false)
  const [loadingClose, setLoadingClose] = useState(false)
  const [isModalOpen, setModalOpen] = useState(false)
  const [isClicked, setClicked] = useState(false)
  const [CurrentCompany, setCurrentCompany] = useState(0)
  const [NewCompany, setNewCompany] = useState(0)
  const [msgNewCompany, setMsgNewCompany] = useState(false)
  const [msgCustomer, setMsgCustomer] = useState('')

  const [enterpriseLoader, setEnterpriseLoader] = useState(true);
  //const [items, setItem] = useState(userInfo.items);
  const [user, setUser] = useState(userInfo.user);
  const [userDetails, setUserDetails] = useState<UserDetails>({
    token: {
      customerId: 0,
      nameId: 0,
    },
    license: false,
  })
  const [enterpriseDetails, setEnterpriseDetails] = useState<Enterprise[]>([])
  const enteredCustomerFilter = ''
  const dispatch = useDispatch();

  //#region   Effects

  /**
   * get user information from token and get enterprises based on that information
   */
  useEffect(() => {
    const result = localStorage.getItem('jwtToken');
    let userResult;
    if (user.name === '') {
      if (result !== null) {
        userResult = parseJwt(result);
        const roles = userResult.IBUHOO_ROLE;
        setUser({
          ...user, email: userResult.email,
          name: `${userResult.given_name} ${userResult.family_name}`,
          role: Array.isArray(roles) ? roles : [roles],
          profilePicture: `${userResult.given_name.charAt(0)}${userResult.family_name.charAt(0)}`
        })
        setCurrentCompany(parseInt(userResult.customer_id))
        setUserDetails({
          token: {
            customerId: userResult.customer_id,
            nameId: userResult.nameid
          },
          license: false
        })
      }
      if (!enterprises.length) {
        if (userResult !== undefined)
          getEnterprise(userResult.nameid, appName, dispatch)
      }
    }
    return () => {
      console.log('clean')
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])


  useEffect(() => {
    debugger
    if (!!enterprises.length && !enterpriseDetails.length) {
      setEnterpriseDetails(enterprises)
      setEnterpriseLoader(false)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [enterprises])


  useEffect(() => {
    debugger
    if (tokens.token !== '') {
      if (
        (tokens.informationCode === 'A10' || tokens.informationCode === 'A20') &&
        tokens.typeOperation === 'OPT02'
      ) {
        const dataUser = parseJwt(tokens.token)
        const namePrivilege = user.name.toUpperCase() + '_PRIVILEGE'
        const appPrivilege = dataUser[namePrivilege]
        const dataXUser = {
          customerName: dataUser.customer_name.toLowerCase(),
          name: dataUser.unique_name.toLowerCase(),
          nameid: dataUser.nameid,
          privileges: appPrivilege,
          surname: dataUser.family_name.toLowerCase(),
          userId: dataUser.jti,
        }
        const totalUserData = JSON.stringify(dataXUser)
        if (dataUser.customer_id !== CurrentCompany) {
          setCurrentCompany(NewCompany)
          localStorage.setItem('jwtToken', `Bearer ${tokens.token}`)
          localStorage.setItem('refreshToken', `${tokens.refreshToken}`)
          localStorage.setItem('userTokenData', totalUserData)
          localStorage.setItem('encryptedToken', `${tokens.urlApp.split('?')[1]}`)
          setMsgCustomer('Se ha cambiado correctamente a la empresa solicitada.')
        } else {
          setMsgCustomer(
            'No se puede cambiar la empresa, la empresa seleccionada no dispone de licenciamiento para esta aplicación.',
          )
        }
      }
      setMsgNewCompany(true)

      dispatch(clearTokenResponse())
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tokens])

  useEffect(() => {
    if (tokenErrors.message !== '') {
      setMsgCustomer(
        'No se puede cambiar la empresa, la empresa seleccionada no dispone de licenciamiento para esta aplicación.',
      )
      dispatch(generateTokensErrors({ message: '', statusCode: 0 }))
      setMsgNewCompany(true)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tokenErrors])


  //#endregion

  const handleChange = (valueCustomer: number) => {
    const token: (any | null) = localStorage.getItem('jwtToken')
    if (valueCustomer !== CurrentCompany) {
      setNewCompany(valueCustomer)
      const tokenCustomer = token.replace('Bearer ', '')
      const bodyCustomer = {
        token: tokenCustomer,
        Option: 'OPT02',
        EnterpriseSelected: valueCustomer,
      }
      generateTokens(bodyCustomer, dispatch)
    }
    setAnchorEl(false)
    setModalOpen(false)
  }

  const handleClick = (event: any) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
    setAnchorElPop(null)
  }

  const handleModalOpen = () => {
    if (isModalOpen) {
      setModalOpen(false)
    } else {
      setModalOpen(true)
    }
  }

  const closeSession = () => {
    setLoadingClose(true)
    //  revokeTokenUsers(setLoadingClose);
  }

  const handleConfirm = () => {
    setConfirmPrincipal(true)
  }

  const handleClickPop = (event: any) => {
    setAnchorElPop(event.currentTarget)
  }

  const handleMouseLeave = () => {
    if (!isClicked) {
      setAnchorElPop(null)
    }
  }

  const handleIsClicked = () => {
    if (CurrentCompany === 0) {
      setCurrentCompany(userDetails.token.customerId)
    }
    getEnterprise(userDetails.token.nameId, user.name, dispatch)
    setClicked(!isClicked)
  }

  const getRowData = (data: Enterprise[]) => {
    const filtered = data.filter(
      (num: Enterprise) =>
        num.license === true &&
        (num.name.toLowerCase().indexOf(enteredCustomerFilter.toLowerCase()) >= 0 || enteredCustomerFilter === ''),
    )
    if (filtered.length <= 0) {
      return <Grid style={{ margin: 10 }}>{'No hay empresas'}</Grid>
    } else {
      return filtered.slice(0, 5).map((row) => {
        const totalName = row['name']
        const splitName = totalName.split(' ')
        let customerName = ''
        if (splitName.length > 1) {
          customerName = splitName[0] + ' ' + splitName[1]
        } else {
          customerName = splitName[0]
        }
        const customerId = row['id']
        return (
          <FormControlLabel
            key={customerId}
            name={'empr' + customerId}
            value={customerId}
            style={{ margin: 5 }}
            control={<StyledRadioButton />}
            label={
              <Tooltip title={totalName}>
                <Typography className={classes.typography} align={'center'}>
                  {customerName}
                </Typography>
              </Tooltip>
            }
          />
        )
      })
    }
  }

  const moreCompanies = (enterprises: { license: boolean }[]) => {
    if (!!enterprises.length) {
      if (enterprises.filter((x) => x.license).length > 5) {
        return (
          <List component="nav" aria-label="otrasEmpresas">
            <ListItem button className={classes.rightButton} onClick={handleModalOpen}>
              <Grid container item lg={12} direction="row" >
                <ListItemText primary="Ver más" />
              </Grid>
              <NestedMenuItem parentMenuOpen={!!anchorEl} />
            </ListItem>
          </List>
        )
      }
    }
  }



  const typographyStyle = {
    // color: theme.palette.primary.main,
    font: 'normal normal normal 14px/19px Muli',
    letterSpacing: '0px',
    cursor: 'pointer',
  }

  return (
    <div>
      <Avatar className="avatar" color="default" onClick={handleClick}>
        {user.profilePicture}
      </Avatar>
      <StyledMenu
        id="customized-menu"
        anchorEl={anchorEl}
        keepMounted
        open={openMenu}
        onClose={handleClose}
        component="div"
        background={'green'}
      >
        <div className={classes.contentProfile}>
          <Grid container justify="center">
            <Grid item container justify="center">
              <AccountCircle style={{ fontSize: 50 }}></AccountCircle>
            </Grid>
            <Grid item container md={12} justify="center">
              <Typography style={{ fontWeight: 'bold' }} className={classes.typography}>
                {user.name}
              </Typography>
            </Grid>
            <Grid item container justify="center">
              <Typography className={classes.typography}>{user.email}</Typography>
            </Grid>
            <Grid item container justify="center">
              <Typography className={classes.typography}>
                <Grid container item direction="row" >
                  Rol: {' '}
                  {
                    <>
                      {user.role.length > 2 ? (
                        <Tooltip title={user.role.toString()}>
                          <Typography align={'center'} style={typographyStyle}>
                            {user.role.length > 1
                              ? `${toPascalCase(user.role[0])}, ${toPascalCase(user.role[1])} ...`
                              : toPascalCase(user.role[0])}
                          </Typography>
                        </Tooltip>
                      ) : (
                          <Typography align={'center'} style={typographyStyle}>
                            {user.role.length > 1
                              ? `${toPascalCase(user.role[0])}, ${toPascalCase(user.role[1])}`
                              : toPascalCase(user.role[0])}
                          </Typography>
                        )}
                    </>
                  }
                </Grid>
              </Typography>
            </Grid>
          </Grid>
        </div>
        <Divider />
        <MenuItem
          key="enterprise_list"
          className={classes.root}
          aria-haspopup="true"
          onClick={handleIsClicked}
          onMouseEnter={handleClickPop}
          onMouseLeave={handleMouseLeave}
          component="div"
        >
          <ListItemIcon>
            <Business fontSize="small" />
          </ListItemIcon>
          <ListItemText primary="Empresas" />
          <NestedMenuItem parentMenuOpen={!!anchorEl}>
            {enterpriseLoader ? (
              <CircularProgress size={20} style={{ marginTop: 5, marginBottom: 5, marginRight: 20, marginLeft: 20 }} />
            ) : (
                <>
                  {' '}
                  <RadioGroup
                    aria-label="CustomTot"
                    value={CurrentCompany}
                    onChange={(e) => handleChange(parseInt(e.target.value))}
                  >
                    {getRowData(!!enterpriseDetails.length ? enterpriseDetails : [])}
                  </RadioGroup>
                  {moreCompanies(enterpriseDetails)}
                </>
              )}
          </NestedMenuItem>
        </MenuItem>
        {userInfo.items?.map((item) => (
          <MenuItem
            id={item.title}
            className={classes.root}
            style={{ "&hover": { background: "blue" } }}
            selected={item.selected}
            {...item.props}
          >
            <ListItemIcon>
              {item.icon}
            </ListItemIcon>
            <ListItemText primary={item.title} />
          </MenuItem>

        ))}

        <MenuItem key="close-session" onClick={handleConfirm} className={classes.root}>
          <ListItemIcon>
            <PowerSettingsNew fontSize="small" />
          </ListItemIcon>
          <ListItemText primary="Cerrar Sesión" />
        </MenuItem>
      </StyledMenu>
      <div>
        <Modal
          open={isModalOpen}
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          onClose={handleModalOpen}
          className={classes.modalEnterprises}
        >
          <div
            style={{
              top: '30%',
              left: '40%',
            }}
            className={classes.paperModal}
          >
            <TextField
              className={classes.searchTextField}
              value={enteredFilter}
              autoFocus={true}
              onChange={(e) => setEnteredFilter(e.target.value)}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon color="inherit" />
                  </InputAdornment>
                ),
              }}
              variant="outlined"
              margin="dense"
            />
            <List className={classes.radioList}>
              {(!!enterpriseDetails.length ? enterpriseDetails : [])
                .sort((a, b) => (a.name === b.name ? 0 : a.name > b.name ? 1 : -1))
                .filter(
                  (temp) =>
                    temp.license === true && temp.name.toString().toLowerCase().includes(enteredFilter.toLowerCase()),
                )
                .map((item, key) => (
                  <>
                    <ListItem key={key} role={undefined} dense button onClick={(e) => handleChange(item.id)}>
                      <FormControlLabel
                        key={item.id}
                        name={'empr2' + item.id}
                        value={item.id}
                        style={{ margin: 5 }}
                        control={<StyledRadioButton />}
                        checked={item.id === CurrentCompany ? true : false}
                        label={item.name}
                      />
                    </ListItem>
                    <Divider />
                  </>
                ))}
            </List>
          </div>
        </Modal>
      </div>
      <div>
        <AlertConfirm
          isOpen={confirmPrincipal}
          modalMessage={`¿Está seguro de cerrar sesión?`}
          handleAccept={() => {
            closeSession()
          }}
          isLoading={loadingClose}
          showBtnAccept={true}
          showBtnCancel={true}
          onClose={() => {
            setConfirmPrincipal(false)
            setAnchorEl(null)
          }}
        />
      </div>
      <div>
        <AlertConfirm
          isOpen={msgNewCompany}
          modalMessage={msgCustomer}
          handleAccept={() => {
            setMsgNewCompany(false)
          }}
          isLoading={loadingClose}
          showBtnAccept={true}
          textButtonAccept={'Aceptar'}
        />
      </div>
    </div>
  )
}


export default UserMenu
