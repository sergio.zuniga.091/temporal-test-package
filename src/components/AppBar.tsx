import React from 'react'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import UserMenu from './UserMenu'
import { Group, Work } from "@material-ui/icons";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  }),
)

export default function ButtonAppBar() {
  const classes = useStyles()
  const userInfo: UserInfo = {
    user: {
      enterprise: '',
      email: '',
      name: '',
      role: [''],
      profilePicture: ''
    },
    items: [
      {
        title: 'Usuarios',
        icon: <Group fontSize="small" />,
        selected: false,
        props: {
          onClick: () => alert('user selected')
        }
      }, {
        title: 'Roles',
        icon: <Work fontSize="small" />,
        selected: false,
        props: {
          onClick: () => alert('roles selected')
        }
      }
    ]
  }

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            Security online
          </Typography>
          <UserMenu key={'menu'} appName={'ibuhoo'} userInfo={userInfo} />
        </Toolbar>
      </AppBar>
    </div>
  )
}
