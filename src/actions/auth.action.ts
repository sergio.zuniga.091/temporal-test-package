import axios, { AxiosRequestConfig } from 'axios'
import { API_AUTHENTICATION } from '../config/config'
import { generateTokensErrors, generateTokensResponse } from '../store/auth.actionCreators'


export const generateTokens = async (data: any, dispatch: any) => {
  debugger
  try {
    const config: AxiosRequestConfig = {
      url: '/api/login/GenerateTokens',
      baseURL: API_AUTHENTICATION,
      method: 'POST',
      data,
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        'Access-Control-Allow-Origin': '*',
        Authorization: data === undefined ? '' : data.token,
      },
    }
    const res = await axios(config)
    if (res.status === 200) {
      dispatch(generateTokensResponse(res.data.result))
    }
  } catch (error) {

    dispatch(generateTokensErrors(error))
  }
}
