import axios, { AxiosRequestConfig } from 'axios'
import { API_SECURITY_ENDPOINT } from '../config/config'
import { Dispatch } from 'redux'
import { enterpriseError, enterprisesResponse } from '../store/enterprise.actionCreators'



/**
 * endpoint para consulta de empresas por usuario
 * @param userName
 * @param appName
 */
export const getEnterprise = async (userId: number, app: string, dispatch: Dispatch) => {
  try {
    const config: AxiosRequestConfig = {
      url: `/api/User/GetCustomerByUser`,
      method: 'GET',
      baseURL: API_SECURITY_ENDPOINT,
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        'Access-Control-Allow-Origin': '*',
        Authorization: `${localStorage.getItem('jwtToken')}`,
        userId,
        app
      },
      transformResponse: [
        (data: any) => {
          data = JSON.parse(data)
          return data.result.customers.map((item: any) => ({
            ...item,
            license: item.licen
          }))
        },
      ],
    }
    const { data } = await axios(config)
    dispatch(enterprisesResponse(data))
  } catch (error) {
    dispatch(enterpriseError(error))
  }
}
