
interface UserInfo {
    user: {
        enterprise: string
        email: string
        name: string
        role: string[]
        profilePicture: string
        userDetails?: UserDetails
    }
    items?: {
        icon?: any
        title: string
        selected?: boolean
        enterprise?: {
            currentEnterprise: number
            enterprises: Enterprise[]
            props?: any
        } | null
        props?: any
    }[]
}


interface Enterprise {
    id: number
    documentType: string
    documentNumber: number
    name: string
    state: number
    license: boolean
}

interface UserDetails {
    token: {
        customerId: number
        nameId: number
    }
    license: boolean
}

type EnterpriseState = {
    enterprises: Enterprise[]
}

type EnterpriseAction = {
    type: string
    payload: Enterprise[]
}

interface CustomError {
    message: string
    statusCode: number
}

type CustomErrorState = {
    error: CustomError
}

type CustomErrorAction = {
    type: string,
    payload: CustomError
}

type TokenState = {
    token: string
    informationCode: string
    typeOperation: string
    refreshToken: string
    urlApp: string
}


type TokenResponseAction = {
    type: string,
    payload: TokenState
}