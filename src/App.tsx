import React from 'react'
import { Provider } from 'react-redux'
import './App.css'
import ButtonAppBar from './components/AppBar'
import { store } from "./store/store";
function App() {
  return (
    <Provider store={store} >
      <ButtonAppBar />
    </Provider>

  )
}

export default App
